function [FORC, Hu, Hc, SF, integral, intM2, Mdiff, HNew, HrNew, sigma, noSlopeM] = calcgFORC(file, interpolSteps, fieldRes, Ncut, useCUDA)

data = load(file, '-MAT');

if isfield(data, 'forc')
    %prepraring the LeXtender FORC file
    
    %create linear H values
    Hmin = min(data.forc(1).data(:,1));
    Hmax = max(data.forc(1).data(:,1));
    
    %create linear Hr values
    for k = 1:size(data.forc,2)
        HrOld(k) = data.forc(k).data(1,1);
    end
    
    %Slightly alter doubled field values in measurement
    [M,H] = removeDoubleH(data.forc);
    
    %Old Hr Matrix for interpolation
    [~,Hr] = meshgrid(ones(1,size(H,2)),HrOld);
    
elseif isfield(data, 'M')
    %prepraring manual input
    %Input needs to be .mat file which includes the 3 n x m matricees H, Hr and M
    
    %create linear H values
    Hmin = min(data.H(:));
    Hmax = max(data.H(:));
    
    H = data.H;
    Hr = data.Hr;
    M = data.M;
end

Hint = linspace(Hmin, Hmax, interpolSteps);
HrInt = Hint;

%create new H and Hr rectangular Matrix for interpolation on a rectangular grid
[HNew,HrNew] = meshgrid(Hint,HrInt);

%cutoff the first values of M
cutM = cutoff(M, Ncut);

%interpolate on recangular meshgrid
intM2 = interpolate2D(H, Hr, cutM, HNew, HrNew);

%remove diamagnetic slope
noSlopeM = rmSlope(intM2, HNew);

%extend FORCs
extendedM = extendFORC(noSlopeM);

%This comes from combining
%https://onlinelibrary.wiley.com/doi/epdf/10.1002/elps.201400475
%and baseline resolution paper. Triangle is 0 at 2 sigma, therefore 4 sigma
%for 2 gaussians next to each other. From baseline resolution paper we can
%calculate the SF from the reolution 4sigma

sigma = fieldRes/4;
SF = (4*sigma*interpolSteps/(Hmax-Hmin)-1)/2;

%caluclate FORC densites via FFTs
[FORC, Mdiff] = calcFORC(extendedM, HNew, HrNew, sigma);


%calculate Hu and Hc coordinates
Hu = 1/2*(HNew + HrNew);
Hc = 1/2*(HNew - HrNew);

%calculate integral
deltaH = HNew(1,1) - HNew(1,2);
deltaHr = HrNew(1,1) - HrNew(2,1);
integral = nansum(nansum(FORC))*deltaH*deltaHr;


%===========================SubFunction==============================

    function [Mtemp,H] = removeDoubleH(forc,H)
        %find double H values
        for i = 1:size(forc,2)
            [~, uniqueIndex, ~] = unique(forc(i).data(:,1));
            for j = 1:size(forc(i).data(:,1))
                if ~ismember(j,uniqueIndex)
                    forc(i).data(j,1) = forc(i).data(j,1) - j*1e-10;
                end
            end
            %and create old H and M matrix for interpolation
            try
                %MOKE FORC
                H(i,:) = forc(i).data(:,1);
                Mtemp(i,:) = forc(i).data(:,2)';
            catch
                %SQUID FORC; Does not have the same amount of measurement points
                %for every minor loop, so we interpolate onto the amount of
                %measurement points in first minor loop
                H(i,:) = linspace(min(forc(i).data(:,1)), max(forc(i).data(:,1)), size(H(1,:),2));
                Mtemp(i,:) = interp1(forc(i).data(:,1),forc(i).data(:,2)',H(i,:));
            end
        end
        
    end


    function cutM = cutoff(M, Ncut)
        for i = 1:size(M,1)
            %skip if all entries are nans
            if all(isnan(M(i,:)))
                continue
            end
            
            %find the last Nan at the beginning of each minor loop
            nans = find(isnan(M(i,:)));
            lastNan = max(nans);
            for j = 1:size(nans,2)-1
                if nans(j) + 1 ~= nans(j+1)
                    lastNan = j;
                end
            end
            
            %Set last nan to 0 if there are no nans
            if isempty(lastNan)
                lastNan = 0;
            end
            
            %cutoff the beginning of the loop
            if lastNan + Ncut + 1 <= size(M,2) && lastNan + Ncut + 1 > 0
                M(i,1:lastNan+Ncut) = NaN;
            elseif lastNan + Ncut + 1 > size(M,2)
                M(i,:) = NaN;
            end
            
        end
        cutM = M;
    end


    function intM2 = interpolate2D(H, Hr, cutM, HNew, HrNew)
        
        intM1 = NaN(size(cutM,1), size(HNew,2));
        intM2 = NaN(size(HNew));
      
        for i = 1:size(cutM, 1)
            
            %remove double H values because that does not work for interpolation
            [intH, index] = unique(H(i,:));
            intM = cutM(i,index);
            
            %remove Nan values for interpolation
            MNan = isnan(intM);
            intM(MNan) = [];
            intH(MNan) = [];

            if length(intM) > 1
                intM1(i,:) = interp1(intH, intM, HNew(1,:));
            end
            
        end
        
        for i = 1:size(intM1, 2)   
            if sum(~isnan(intM1(:,i))) > 1
                intM2(:,i) = interp1(Hr(:,1),intM1(:,i), HrNew(:,1));
            end
        end

    end


    function extendedM = extendFORC(M)
        for i = 1:size(M,1)
            %skip if all entries are nans
            if all(isnan(M(i,:)))
                continue
            end
            
            %find the last Nan at the beginning of each minor loop
            nans = find(isnan(M(i,:)));
            lastNan = max(nans);
            for j = 1:size(nans,2)-1
                if nans(j) + 1 ~= nans(j+1)
                    lastNan = j;
                end
            end
            
            %extend FORCs by 10%
            extension = round(size(M,2)*0.1);
            
            if lastNan > extension
                beginning = lastNan-extension;
            else
                beginning = 1;
            end
            
            if lastNan+1 <= size(M,2)
                M(i,beginning:lastNan) = M(i,lastNan+1);
            else
                M(i,:) = NaN;
            end
            
        end
        extendedM = M;
    end


    function intM2 = rmSlope(intM2, H)
        
        avgLoop = nanmean(intM2,1);
        avgLoop = avgLoop(~isnan(avgLoop));
        
        fitPoints = round(length(avgLoop)/10);
        fit = polyfit(H(1,end-fitPoints:end),avgLoop(end-fitPoints:end),1);
        
        intM2 = intM2 - fit(1) * H;
        
    end


    function [FORC, Mdiff] = calcFORC(M, H, Hr, sigma)
        Mdiff = FFTFilterDiff(M, H, sigma);
        
        %rotate by 270 deg for next derivation
        Mdiff = rot90(Mdiff,3);
        
        %Do the second derivative and filtering
        FORCdens = (FFTFilterDiff(Mdiff, Hr', sigma));
        
        %rotate back to normal and divide by 2 for FORC density
        FORC = 1/2*rot90(FORCdens);
        
    end


    function FORCDiff = FFTFilterDiff(Matrix, H, sigma)
        %initialize Variable
        FORCDiff = NaN(size(Matrix));
        H = H(1,:);
        for i = 1:size(Matrix,1)
            %Remove Nans for FFT and make Signal periodic
            if all(isnan(Matrix(i,:)))
                continue
            else
                
                noNan = Matrix(i,~isnan(Matrix(i,:)));
                if useCUDA
                    forFFT = gpuArray([noNan fliplr(noNan)]);
                else
                    forFFT = [noNan fliplr(noNan)];
                end
                
                %calculate "reciprocal H lattice"
                L = size(forFFT,2);
                
                %Hmax and Hmin have to be adapted to the corresponding minor
                %loop, otherwise the integrated FORC density is different,
                %since the H in the multiplication for the derivation in the
                %FFT changes the volume
                HMax=max(H(end-L/2+1:end));
                HMin=min(H(end-L/2+1:end));
                
                %skip this minor loop if min and max field is equal
                if isequal(HMax,HMin)
                    continue
                end
                
                Hs = 2*pi/(2*(HMax-HMin))*[0:L/2-1 0 -L/2+1:-1];
                
                %filter for fourier space, sigma in matlab is just 1/sigma in
                %fourier space
                if useCUDA
                    filter = gpuArray(gaussmf(Hs, [1/sigma 0]));
                else
                    filter = gaussmf(Hs, [1/sigma 0]);
                end
                
                %fft
                fftTemp = fft(forFFT);
                
                %filtering in Fourier Space
                tempFilter = filter([1:size(fftTemp,2)/2 end-size(fftTemp,2)/2+1:end]);
                fftTemp = fftTemp .* tempFilter;
                
                %Derivation in Fourier Space and back transformation
                fftTempDeriv = ifft(1i*Hs.*fftTemp);
                
                %read nans and sort back into matrix
                FORCdensTemp = NaN(1,size(Matrix,2));
                if useCUDA
                    FORCdensTemp(1,~isnan(Matrix(i,:))) = gather(real(fftTempDeriv(1:size(fftTempDeriv,2)/2)));
                else
                    FORCdensTemp(1,~isnan(Matrix(i,:))) = real(fftTempDeriv(1:size(fftTempDeriv,2)/2));
                end
                
                FORCDiff(i,:) = FORCdensTemp;
            end
        end
    end

end