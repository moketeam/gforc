# gFORC

## A GPU Accelerated First-Order Reversal-Curve Calculator

gFORC is a MATLAB 2019a based First-Order Reversal-Curve (FORC) evaluation program capable of speeding up FORC calculations by a factor of 1000. The program was developed at the Max Planck Institute for Intelligent Systems in Stuttgart and is written and maintained by Felix Groß and Joachim Gräfe. As an open source software, feel free to modify and adapt the code for your personal needs under the MIT License. In case of using gFORC for one of your publications, we kindly ask you to cite Groß et al. J. Appl. Phys. 126, 163901 (2019)<placeholder><br />

#### Files:

**gFORC.fig** - MATLAB GUI file (mandatory)<br />
**gFORC.m** - MATLAB GUI file (mandatory)<br />
**calcgFORC.m** - MATLAB function file doing the actual FORC calculation (mandatory)<br />
**myWaitbar.m** - MATLAB function file to construct the wait bar (mandatory)<br />
**colorbar.mat** - MATLAB file storing the color map (mandatory)<br />
**dummy.mat** - MATLAB file storing an example file of a data set (optional)<br />

#### How to use:

Typing **'gFORC'** in the MATLAB command line should open the gFORC GUI. A FORC data set can be loaded using the **'Load File'** button. The software can handle two different kinds of files both of which are MATLAB native:
- LeXtender FORC acquisition files.
- MATLAB *.mat files which include H, Hr and M in the form of (n x m) matrices. The first direction of each matrix has to be Hr, second one H.

The **'Resolution'** text field is given in actual measurement units. It accepts single inputs (i.e. '13.47') but can also arrays as long as they are given in a MATLAB native format (i.e. '[3 4 5]', 'linspace(1, 100, 500)', '1:0.1:10').<br />
The **'Interpol'** text field specifies the amount of grid interpolation steps from -Hmax to +Hmax. The amount of grid points should not exceed the amount of measurement points in any grid direction.<br />
The **'Cutoff'** determines the amount of data points to omit at the beginning of each minor loop. This can be necessary for some measurement techniques. If possible, the cutoff should be 0.<br />
The **'Try CUDA GPU calculation'** check box can be checked to move the FORC calculation onto a NVIDIA GPU. Depending on the GPU/CPU this can lead to a reduction of computing time.<br />
**'Process'** starts the FORC calculation with the given parameters.<br />

A loading bar will appear on the right side of the GUI. After the processing is done, the GUI will display a list of different smoothing factors. These smoothing factors are calculated from the given resolutions and the grid interpolation steps.<br />
These given smoothing factors are the equivalent of the resolution to the conventional smoothing factor of the FORC technique. When clicking on one of the smoothing factors, the corresponding FORC will be plotted and the current resolution and smoothing factors are displayed to the right side of the process button.<br />
Using the four different text fields around the FORC axis, the axes limits can be adjusted.<br />
The **'Autocorrelate'** button on the top right, calculates the autocorrelation function of each of the FORCs listed in the current list. The **'Neighbors'** text field determines the amount of neighbors to include for the autocorrelation. After the calculation, the result will be displayed in the axis below.<br />
The FORC and autocorrelation data can be exported using the **'Save Data'** button. If a autocorrelation has been calculated, it is included in the save file. Be careful, 500 FORC already produce several GB of data, depending on the amount of interpolation steps. 


