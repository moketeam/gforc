function varargout = gFORC(varargin)
%gFORC MATLAB code file for gFORC.fig
%      gFORC, by itself, creates a new gFORC or raises the existing
%      singleton*.
%
%      H = gFORC returns the handle to a new gFORC or the handle to
%      the existing singleton*.
%
%      gFORC('Property','Value',...) creates a new gFORC using the
%      given property value pairs. Unrecognized properties are passed via
%      varargin to gFORC_OpeningFcn.  This calling syntax produces a
%      warning when there is an existing singleton*.
%
%      gFORC('CALLBACK') and gFORC('CALLBACK',hObject,...) call the
%      local function named CALLBACK in gFORC.M with the given input
%      arguments.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help gFORC

% Last Modified by GUIDE v2.5 05-Sep-2019 16:25:46

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
    'gui_Singleton',  gui_Singleton, ...
    'gui_OpeningFcn', @gFORC_OpeningFcn, ...
    'gui_OutputFcn',  @gFORC_OutputFcn, ...
    'gui_LayoutFcn',  [], ...
    'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before gFORC is made visible.
function gFORC_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   unrecognized PropertyName/PropertyValue pairs from the
%            command line (see VARARGIN)

% Choose default command line output for gFORC
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes gFORC wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = gFORC_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;



function editResolution_Callback(hObject, eventdata, handles)
% hObject    handle to editResolution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editResolution as text
%        str2double(get(hObject,'String')) returns contents of editResolution as a double


% --- Executes during object creation, after setting all properties.
function editResolution_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editResolution (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listboxSF.
function listboxSF_Callback(hObject, eventdata, handles)
% hObject    handle to listboxSF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listboxSF contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listboxSF

%get clicked object and replot that FORC, also update num
get(hObject,'Value');
handles.num = get(hObject,'Value');
plotFORC(handles)

guidata(hObject, handles);



% --- Executes during object creation, after setting all properties.
function listboxSF_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listboxSF (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editInterpol_Callback(hObject, eventdata, handles)
% hObject    handle to editInterpol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editInterpol as text
%        str2double(get(hObject,'String')) returns contents of editInterpol as a double


% --- Executes during object creation, after setting all properties.
function editInterpol_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editInterpol (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editCutoff_Callback(hObject, eventdata, handles)
% hObject    handle to editCutoff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editCutoff as text
%        str2double(get(hObject,'String')) returns contents of editCutoff as a double


% --- Executes during object creation, after setting all properties.
function editCutoff_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editCutoff (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in buttonLoadfile.
function buttonLoadfile_Callback(hObject, eventdata, handles)
% hObject    handle to buttonLoadfile (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
%let user select data file

try
    [dataFile, dataPath] = uigetfile({'*.forc','LeXtender FORC Acquisition File';'*.mat','MAT-file (H, Hr, M)'}, 'Select LeXtender FORC Aquisition File', handles.filepath);
catch
    [dataFile, dataPath] = uigetfile({'*.forc','LeXtender FORC Acquisition File';'*.mat','MAT-file (H, Hr, M)'}, 'Select LeXtender FORC Aquisition File', 'D:\');
end

if dataFile == 0
    return
end
try
    %load data file
    handles.filename = fullfile(dataPath, dataFile);
    handles.filepath = dataPath;
    
    set(handles.textFilename, 'string', handles.filename)
    guidata(hObject, handles);
    
catch errLoad
    disp(errLoad)
    return
end

% --- Executes on button press in buttonProcess.
function buttonProcess_Callback(hObject, eventdata, handles)
% hObject    handle to buttonProcess (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

%get input parameters from text fields

handles.proc = [];
interpolSteps = str2num(get(handles.editInterpol, 'string'));
fieldRes = str2num(get(handles.editResolution, 'string'));
Ncut = str2num(get(handles.editCutoff, 'string'));
file = handles.filename;

%do FORC calculations and store data in proc field of handle struc
SFs = [];


handles.axesWaitbar = axes('visible', 'off');
set(handles.axesWaitbar, 'position', [0.58 0.73 0.35 0.025]);


if gpuDeviceCount > 0 && get(handles.checkboxGPU, 'value')
    useCUDA = 1;
else
    useCUDA = 0;
end

timer = tic;
for i = 1:length(fieldRes)
    
    try
        myWaitbar(handles, i, length(fieldRes)-1)
    catch
    end
    

    [FORC, Hu, Hc, SF, integral, intM2, Mdiff, HNew, HrNew, sigma, noSlopeM] = calcgFORC(file, interpolSteps, fieldRes(i), Ncut, useCUDA);
    handles.proc(i).fieldRess = fieldRes(i);
    handles.proc(i).FORCs = FORC;
    handles.proc(i).Hus = Hu;
    handles.proc(i).Hcs = Hc;
    handles.proc(i).SFs = SF;
    SFs = [SFs SF];
    handles.proc(i).integrals = integral;
    handles.proc(i).intM2s = intM2;
    handles.proc(i).Mdiffs = Mdiff;
    handles.proc(i).HNews = HNew;
    handles.proc(i).HrNews = HrNew;
    handles.proc(i).sigmas = sigma;
    handles.proc(i).noSlopeM = noSlopeM;
end

%show calculation time
calcTime = seconds(round(toc(timer)));
set(handles.textCalcTime, 'string', ['Calculation Time: ' datestr(calcTime,'HH:MM:SS')])

delete(handles.axesWaitbar)

%plot first FORC
handles.num = 1;
plotFORC(handles)

%also after first processing: turn list and SF and
set(handles.listboxSF, 'Visible', 'on')
set(handles.listboxSF,'value',1);
set(handles.listboxSF,'string',SFs);


guidata(hObject, handles);




function plotFORC(handles)
%function to plot FORC in FORC axes

axes(handles.axesFORC)

%get plot data from proc variable
FORC = handles.proc(handles.num).FORCs;
Hu = handles.proc(handles.num).Hus;
Hc = handles.proc(handles.num).Hcs;
SF = handles.proc(handles.num).SFs;
fieldRes = handles.proc(handles.num).fieldRess;

%plot stuff and reset the font type
surf(Hc, Hu, FORC, 'edgecolor', 'none')
set(handles.axesFORC,'FontName','Segoe UI Light')

%make plot nice
view(2)
load colorbar.mat customMap
colormap(gca, customMap)
lim = max([abs(min(min(FORC))) abs(max(max(FORC)))]);
caxis([-lim lim])
box on
set(gca, 'Layer', 'top')
xlabel('{\it H}_c [Oe/T]')
ylabel('{\it H}_u [Oe/T]')

%after plotting always set limits
editXLim1_Callback(handles.editXLim1, [], handles)
editXLim2_Callback(handles.editXLim2, [], handles)
editYLim1_Callback(handles.editYLim1, [], handles)
editYLim2_Callback(handles.editYLim2, [], handles)

%adjust text handles for SF and field Res
set(handles.textCurrentRes, 'Visible', 'on', 'String', ['SF = ' sprintf('%1.2f', SF) '   /   ' 'Field Resolution = ' sprintf('%1.2f', fieldRes) ' Oe/T'])




function editXLim1_Callback(hObject, eventdata, handles)
% hObject    handle to editXLim1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editXLim1 as text
%        str2double(get(hObject,'String')) returns contents of editXLim1 as a double
try
    limX = get(handles.axesFORC, 'XLim');
    limX(1) = str2num(get(handles.editXLim1, 'String'));
    set(handles.axesFORC, 'XLim', limX)
catch
    set(handles.editXLim1, 'String', 'NaN')
    set(handles.editXLim2, 'String', 'NaN')
    set(handles.axesFORC, 'XLimMode', 'auto')
end

% --- Executes during object creation, after setting all properties.
function editXLim1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editXLim1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editXLim2_Callback(hObject, eventdata, handles)
% hObject    handle to editXLim2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editXLim2 as text
%        str2double(get(hObject,'String')) returns contents of editXLim2 as a double
try
    limX = get(handles.axesFORC, 'XLim');
    limX(2) = str2num(get(handles.editXLim2, 'String'));
    set(handles.axesFORC, 'XLim', limX)
catch
    set(handles.editXLim1, 'String', 'NaN')
    set(handles.editXLim2, 'String', 'NaN')
    set(handles.axesFORC, 'XLimMode', 'auto')
end

% --- Executes during object creation, after setting all properties.
function editXLim2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editXLim2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editYLim1_Callback(hObject, eventdata, handles)
% hObject    handle to editYLim1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editYLim1 as text
%        str2double(get(hObject,'String')) returns contents of editYLim1 as a double
try
    limY = get(handles.axesFORC, 'YLim');
    limY(1) = str2num(get(handles.editYLim1, 'String'));
    set(handles.axesFORC, 'YLim', limY)
catch
    set(handles.editYLim1, 'String', 'NaN')
    set(handles.editYLim2, 'String', 'NaN')
    set(handles.axesFORC, 'YLimMode', 'auto')
end

% --- Executes during object creation, after setting all properties.
function editYLim1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editYLim1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editYLim2_Callback(hObject, eventdata, handles)
% hObject    handle to editYLim2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editYLim2 as text
%        str2double(get(hObject,'String')) returns contents of editYLim2 as a double
try
    limY = get(handles.axesFORC, 'YLim');
    limY(2) = str2num(get(handles.editYLim2, 'String'));
    set(handles.axesFORC, 'YLim', limY)
catch
    set(handles.editYLim1, 'String', 'NaN')
    set(handles.editYLim2, 'String', 'NaN')
    set(handles.axesFORC, 'YLimMode', 'auto')
end

% --- Executes during object creation, after setting all properties.
function editYLim2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editYLim2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function editNeighbours_Callback(hObject, eventdata, handles)
% hObject    handle to editNeighbours (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editNeighbours as text
%        str2double(get(hObject,'String')) returns contents of editNeighbours as a double


% --- Executes during object creation, after setting all properties.
function editNeighbours_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editNeighbours (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



% --- Executes on button press in pushbuttonAutoCorr.
function pushbuttonAutoCorr_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonAutoCorr (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


interpolSteps = str2num(get(handles.editInterpol, 'string'));
fieldRes = str2num(get(handles.editResolution, 'string'));
Ncut = str2num(get(handles.editCutoff, 'string'));
file = handles.filename;

if gpuDeviceCount > 0 && get(handles.checkboxGPU, 'value')
    useCUDA = 1;
else
    useCUDA = 0;
end

[FORC, Hu, Hc, SF, integral, intM2, Mdiff, HNew, HrNew, sigma, noSlopeM] = calcgFORC(file, interpolSteps, 0, Ncut, useCUDA);

MIntNorm = calcMInt(FORC, HNew);

neighbours = str2num(get(handles.editNeighbours, 'string'));
SFs = [];

handles.axesWaitbar = axes('visible', 'off');
set(handles.axesWaitbar, 'position', [0.58 0.73 0.35 0.025]);
for i = 1:length(handles.proc)
    
    try
        myWaitbar(handles, i, length(fieldRes)-1)
    catch
    end
    
    
    FORC = handles.proc(i).FORCs;
    HNew = handles.proc(i).HNews;
    SFs = [SFs handles.proc(i).SFs];
    %calcMInt does not take into account for the shift between initial
    %magnetization and integrated magnetization (1/2 Ms), that does not matter
    %because calcCorr substracts the mean anyway
    resi = MIntNorm - calcMInt(FORC, HNew);
    
    corr(:,:,i) = calcCorr(resi,neighbours);
    
    stdResi(i) = std(resi(:));
end

delete(handles.axesWaitbar)

I = interpolSteps^2*squeeze(sum(sum(corr,2),1)) / (neighbours^2*nansum(resi(:).^2));
E = -1/(interpolSteps^2-1);
S = sqrt((interpolSteps^4*neighbours^2 + 3*(neighbours^2)^2 - interpolSteps^2 * neighbours^3) / ((interpolSteps^4-1)*((neighbours^2)^2)));


corrSF = (I-E)/S;

%-------------Plot in second chart-----------
axes(handles.axesAutoCorr)

%plot stuff and reset the font type
plot(SFs, corrSF,'color', 'b', 'linewidth', 2)
xlim([-0.02*max(SFs)+min(SFs) 1.02*max(SFs)])
ylim([-0.02*max(corrSF)+min(corrSF) 1.02*max(corrSF)])


set(handles.axesAutoCorr,'FontName','Segoe UI Light')

box on
set(gca, 'Layer', 'top')
xlabel('{\it SF}')
ylabel('Correlation [a.u.]')


for i = 1:length(corrSF)
    handles.proc(i).corr = corrSF(i);
end
guidata(hObject, handles);



function zeroDer = calcMInt(FORC, H)

deltaH = H(1,2)-H(1,1);
firstDer = flipud(cumsum(flipud(-2*FORC), 'omitnan'));
zeroDer = fliplr(cumsum(fliplr(firstDer),2, 'omitnan'));
zeroDer = zeroDer * deltaH^2;



function corrMat = calcCorr(resi, neighbours)
corrMat = zeros(size(resi));
resi = resi - nanmean(resi(:));

for i = 1:neighbours
    
    for j = 1:neighbours
        resiTemp = circshift(resi,i,1);
        resiTemp = circshift(resiTemp,j,2);
        resiTemp(1:i,:) = NaN;
        resiTemp(:,1:j) = NaN;
        
        tempCorr = resi.*resiTemp;
        
        corrMat(i,j) = nansum(tempCorr(:));%*1/sqrt(i^2+j^2);
    end
end



% --- Executes on button press in pushbuttonSave.
function pushbuttonSave_Callback(hObject, eventdata, handles)
% hObject    handle to pushbuttonSave (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

try
    [saveFile, savePath] = uiputfile({'*.gforc','GFORC File'}, 'Select GFORC file', handles.datapath);
catch
    [saveFile, savePath] = uiputfile({'*.gforc','GFORC File'}, 'Select GFORC file', 'D:\');
end

if saveFile == 0
    return
end

try
    
    %save data file
    handles.savename = fullfile(savePath, saveFile);
    handles.savepath = savePath;
    
    
    temp = handles.proc;
    save(handles.savename, 'temp', '-v7.3')
    
    guidata(hObject, handles);
    
catch errLoad
    disp(errLoad)
    return
end


% --- Executes on button press in checkboxGPU.
function checkboxGPU_Callback(hObject, eventdata, handles)
% hObject    handle to checkboxGPU (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hint: get(hObject,'Value') returns toggle state of checkboxGPU
