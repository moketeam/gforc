function myWaitbar(handles, i, n)

load colorbar.mat customMap

y = [0 1];
x = linspace(0,1,size(customMap,1));
z = repmat(x,2,1);

nplot= max([2 round(i/n*length(x))]);

surf(handles.axesWaitbar, x(1:nplot),y,z(:,1:nplot), 'edgecolor', 'none')
caxis([min(z(:)) max(z(:))])

set(handles.axesWaitbar, 'colormap', customMap, 'color', [0.8 0.8 0.8],...
    'XTick', [], 'YTick', [], 'XLim', [-0.015 1.015], 'YLim', [-0.3 1.25],...
    'box', 'on', 'Layer','top', 'linewidth', 2.5)

view(2)

drawnow
